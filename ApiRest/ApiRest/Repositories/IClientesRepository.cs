﻿using ApiRest.Models;

namespace ApiRest.Repositories
{
    public interface IClientesRepository
    {
        Task<IEnumerable<Cliente>> Get();

        Task<Cliente> Get(int id);

        Task<Cliente> Create(Cliente cliente);

        Task Update(Cliente cliente);

        Task Delete(int id); 
    }
}
