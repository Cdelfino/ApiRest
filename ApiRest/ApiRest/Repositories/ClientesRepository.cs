﻿using ApiRest.Models;
using Microsoft.EntityFrameworkCore;

namespace ApiRest.Repositories
{
    public class ClientesRepository : IClientesRepository
    {
        public readonly _ClienteContext _clienteContext;

        public ClientesRepository(_ClienteContext clienteContext)
        {
            this._clienteContext = clienteContext;
        }

        public async Task<Cliente> Create(Cliente cliente)
        {
            _clienteContext.Clientes.Add(cliente);
            await _clienteContext.SaveChangesAsync();

            return cliente;                
        }

        public async Task Delete(int id)
        {
            var clienteDelete = await _clienteContext.Clientes.FindAsync(id);
            _clienteContext.Clientes.Remove(clienteDelete);
            await _clienteContext.SaveChangesAsync();   
        }

        public async Task<IEnumerable<Cliente>> Get()
        {
            return await _clienteContext.Clientes.ToListAsync();
        }

        public async Task<Cliente> Get(int id)
        {
            var _id = await _clienteContext.Clientes.FindAsync(id);
            await _clienteContext.SaveChangesAsync();
            return _id;     
        }

        public async Task Update(Cliente cliente)
        {
            _clienteContext.Entry(cliente).State = EntityState.Modified;
            await _clienteContext.SaveChangesAsync();

        }
    }
}
