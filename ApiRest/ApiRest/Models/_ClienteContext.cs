﻿using Microsoft.EntityFrameworkCore;

namespace ApiRest.Models
{
    public class _ClienteContext : DbContext
    {
        public _ClienteContext(DbContextOptions<_ClienteContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
        public DbSet<Cliente> Clientes  { get; set; }

    }
}
