﻿using System.ComponentModel.DataAnnotations;

namespace ApiRest.Models
{
    public class Cliente
    {
        [Key]
        public int Id { get; set; }
        [Required,MaxLength(120)]
        public string Nome { get; set; }
        [Required, MaxLength(120)]
        public string Departamento { get; set; }
    }
}
