﻿using ApiRest.Models;
using ApiRest.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiRest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientesController : ControllerBase
    {
        public readonly IClientesRepository _clientesRepository;

        public ClientesController(IClientesRepository clientesRepository)
        {
            _clientesRepository = clientesRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<Cliente>> Get()
        {
            return await _clientesRepository.Get();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Cliente>> Get(int id)
        {
            return await _clientesRepository.Get(id);
        }

        [HttpPost]
        public async Task<ActionResult<Cliente>> Post([FromBody] Cliente cliente)
        {
            var newCliente = await _clientesRepository.Create(cliente);
            return newCliente;
        }

        [HttpPut]
        public async Task<ActionResult<Cliente>> Put([FromBody]Cliente cliente,int id)
        {
            if (id != cliente.Id)
            {
                return BadRequest();
            }
            else
            {
                await _clientesRepository.Update(cliente);
                return NoContent();
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Cliente>> Delete(int id)
        {
            var clienteDelete = await _clientesRepository.Get(id);
            
            if(clienteDelete == null)
            {
                return NotFound();
            }
            else {
                await _clientesRepository.Delete(clienteDelete.Id);
                return NoContent();
            }
            
        }


    }


}
